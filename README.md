<inertia ixx="0.02229947916" ixy = "0" ixz = "0" iyy="0.02229947916" iyz = "0" izz="0.034515625" /> <!-- Originally cylinder_inertia above-->
<inertia ixx="0.02262041666" ixy = "0" ixz = "0" iyy="0.02262041666" iyz = "0" izz="0.033930625" /> <!-- Originally cylinder_inertia above-->

roslaunch rbvogui_sim_bringup rbvogui_complete.launch robot_model:=rbvogui robot_xacro:=rbvogui_std_ur10.urdf.xacro launch_arm:=true arm_manufacturer:=ur arm_model:=ur10e
ROS_NAMESPACE=robot rosrun rqt_joint_trajectory_controller rqt_joint_trajectory_controller

##FOR NAVIGATION
roslaunch rbvogui_sim_bringup rbvogui_complete.launch robot_model:=rbvogui robot_xacro:=rbvogui_std.urdf.xacro run_localization:=true run_navigation:=true

#Navigation with your own map
roslaunch rbvogui_sim_bringup rbvogui_complete.launch robot_model:=rbvogui robot_xacro:=rbvogui_std.urdf.xacro run_localization:=true run_navigation:=true map_file:=demo_map/demo_map.yaml


##FOR MAPPING
roslaunch rbvogui_sim_bringup rbvogui_complete.launch robot_model:=rbvogui robot_xacro:=rbvogui_std.urdf.xacro run_mapping:=true rviz_config_file:=rviz/rbvogui_map.rviz

When the map is fine, open a terminal and go to the rbvogui_localization package
cd ~/catkin_ws && source devel/setup.bash
roscd rbvogui_localization && cd maps

Create a folder with the name of the map. For example:
mkdir demo_map
cd demo_map

Finally, save the map inside that folder
ROS_NAMESPACE=robot rosrun map_server map_saver -f demo_map


##LASER VISUALIZATION
roslaunch rbvogui_sim_bringup rbvogui_complete.launch robot_model:=rbvogui use_gpu:=false


##TO MAKE THE ROBOT MOVE TO A GOAL USING MAP (PYTHON)
Launch the robot, localization and navigation:
roslaunch rbvogui_sim_bringup rbvogui_complete.launch robot_model:=rbvogui run_localization:=true run_navigation:=true

Then, run the script. The robot will move to (1,1) position:
ROS_NAMESPACE=robot rosrun rbvogui_gazebo move_robot.py

##OMRON LD90
https://github.com/OmronAPAC/Omron_LD_ROS_Package/blob/master/docs/DeveloperGuide.adoc#getting-started

#git basic commands
cd ~/vogui_ws/src/vogui_packages
git add .
git commit -m "first commit - basic packages"
git push origin main
